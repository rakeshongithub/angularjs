var demoApp = angular.module("myApp", ['ngRoute', 'ngAnimate']);

// Routing configration
demoApp.config(['$routeProvider', function(routeProvider){
	routeProvider
		.when('/home', {
					controller: 'customerCtrl',
					templateUrl: 'views/index.html'
				})
		.when('/fathers', {
					controller: 'customerCtrl',
					templateUrl: 'views/fathers.html'
				})
		.when('/customers',{
					controller: 'emplyCtrl',
					templateUrl: 'views/customers.html'
				})
		.otherwise({redirectTo:'/home'});
}]);

// Father list controler
demoApp.controller("customerCtrl", ['$scope', '$http', function(scope, http){
	http.get("json/fathers.json")
		.success(function(data){
			scope.fathers = data.fathers;
		});

	// Delete Specific Father from list
	scope.removeFather = function(custm){
		var index = scope.fathers.indexOf(custm);
			scope.fathers.splice(index, 1);
	};
}]);

// Navigations controler
demoApp.controller("nvagiationCtrl", ['$scope', '$location', function(scope, location){

	scope.getClass = function(path){
		//console.log(location.path().substr(0, path.lastIndexOf('/')+1))
		if(location.path().substr(0, path.length) == path){
			return true;
		} else {
			return false;
		}
	}
}]);

// Employe list controler
/*demoApp.controller("emplyCtrl", ['$scope', '$http', function(scope, http){
	http.get("json/customers.json")
		.success(function(data){
			scope.customers = data;
		})

}]);*/

/*demoApp.factory('simpleFactory', ['$http', function(http){
	
	var customers = [];

	http.get("json/customers.json")
		.success(function(data){
		customers = data;
	});

	var factory = {};
	
	factory.getCustomer = function(){
		return customers;
	};

	return factory;
}]);*/

demoApp.factory('simpleFactory', ['$http', function($http){
    return{
        	getCustomer: function(){
            	return $http.get("json/customers.json")
        	}
    }
}]);


demoApp.controller("emplyCtrl", ['$scope', 'simpleFactory', function($scope, simpleFactory){
	simpleFactory.getCustomer()
			.success(function(data){
            	$scope.customers = data;
            });
}]);