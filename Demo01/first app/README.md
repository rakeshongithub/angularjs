Angular JS
==========

Let's imagine that we were asked to create a form where a user can type in a short message, and then send it by clicking on a button. There are some additional user-experience (UX) requirements such as message size should be limited to 100 characters, and the Send button should be disabled if this limit is exceeded. A user should know how many characters are left as they type. If the number of remaining characters is less than ten, the displayed number should change the display style to warn users. It should be possible to clear text of a provided message as well. A
finished form looks similar to the following code


##### What's in myapp.js

```javascript
// created angular module in myapp.js files
var app = angular.module('myApp', []);
```

##### What's in appctrl.js
```javascript
// created controller for different actions
app.controller("myCtrl", function($scope){
	// defined default message variable
	$scope.message = "";

	// created function for remaining number of text
	$scope.remaing = function(){return 100 - $scope.message.length};

	// function to save the message
	$scope.save = function(){
						$scope.message = "";
						alert("Data Not Saved!");
				};
	// function to clear the message box
	$scope.clear = function(){$scope.message = ""};

	// function to warn the user, less than 10 character limit has remain
	$scope.warning = function(){ return $scope.remaing() < 11 };

	// function to disable the button if user cross the character limit.
	$scope.btnDisable = function(){ return $scope.message.length >= 101 };
});
```
